# Игнорировать все в директории .terraform расположенной на любой глубине относительно текущиего пути
**/.terraform/*

# файлы или директории с суффиксом .tfstate и маской *.tfstate.*
*.tfstate
*.tfstate.*

# Crash log файл
crash.log

# файлы иди директории с суффиксом .tfvars
*.tfvars

# файлы override.tf override.tf.json и с префиксной маской *_.
override.tf
override.tf.json
*_override.tf
*_override.tf.json

# явно файлы
.terraformrc
terraform.rc
